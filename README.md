# HandFontSSW

HandFontSSW is project that aims to produce fully functional [Sutton SignWriting](http://www.signwriting.org/) font, that tries to look like handwritten. It will happen in steps.
1.  Providing SVGs for individual symbols.
2.  Creating 1D font compliant with Unicode encoding.
3.  Chosing which encoding is the best for 2D or creating our own, if none happens to be usable.
4.  Creating 2D font (probably in Graphite, and then in Universal Shaping Engine?)
5.  Spreading of the font through Internet.

Sadly, I don't have much free time, so this will progress, but it may be slow.
In addition I don't know much about creating fonts.
For now, I'm creating SVGs in Inkscape.